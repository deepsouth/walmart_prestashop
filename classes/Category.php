<?php

require_once __DIR__.'/ToolDb.php';

class ProcessorCategory {

	protected $defaultLang;
	protected $shop;
	protected $key;
	private $langs;
	private $def;
	private $current;
	private $currentIns;
	private $cache;
	private static $FIELD_WALMART = 'walmart_id';
	function __construct($key){
		$context = Context::getContext();
		$this->langs = Language::getLanguages();
		$this->default_lang = $context->language->id;
		$this->shop = $context->shop->id;
		$this->key = $key;
		$this->load_meta();
	}

	function load_meta() {
		$this->def = ObjectModel::getDefinition('Category');
	}

	protected function check_table() {
		$fields = $this->def['fields'];
		if (isset($fields[self::$FIELD_WALMART])) {
			ToolDb::createField($this->def['table'], self::$FIELD_WALMART, 'INT');
		}
	}

    public function get_categories() {
		if (!$this->key || strlen($this->key)==0){
			return [];
		}
		try {
			if (!$this->cache) { 
        		$url_categorias = "http://api.walmartlabs.com/v1/taxonomy?apiKey=$this->key";
				$data = file_get_contents($url_categorias); // llenamos data con el JSON devuelto por la url
				$this->cache = json_decode($data)->categories; // decodificamos data
			}
			return $this->cache;
		} catch (Exception $e) {
			return [];
		}
	}
	
	private function analice_categories($categories, $parent = null) {
		$res = [];
		foreach($categories as $category) {
			$key = $category->id;
			$res[$key] = ["data"=> $category, "parent"=>$parent];
			if (property_exists($category, "children")) {
				$sub_categories = array_merge($res, $this->analice_categories(
					$category->children,
					$parent == null? $category : $parent
				));
				$res = $res + $sub_categories;
			}
		}
		return $res;
	}

	public function get_dict() {
		$categories = $this->get_categories();
		return $this->analice_categories($categories);
	}

	public function selection_process($categories, &$res = []) {
		foreach ($categories as $category) {
			$res[$category->id] = ["gain"=>null, "max"=>null, "enable"=>true];
			if (property_exists($category, 'children')) {
				$res = $this->selection_process($category->children, $res);
			}
		}
		return $res;
	}

	public function inicialice_selection() {
		$categories = $this->get_categories();
		return $this->selection_process($categories);
	}

	public function process_category($category, $parent = 2) {
		$id = CategoryWalmart::getByWalmartId($category->id);
		$ps_category = new CategoryWalmart($id ? $id : null);
		$name = [];
		$description = [];
		$rewrite = [];
		foreach ($this->langs as $lang) {
			$id_lang = $lang['id_lang'];
			if (isset($ps_category->name[$id_lang])) {
				$name[$id_lang] = $ps_category->name[$id_lang];
				$description[$id_lang] = $ps_category->description[$id_lang];
				$rewrite[$id_lang] = $ps_category->link_rewrite[$id_lang];
			} else {
				$name_str = html_entity_decode($category->name);
				$rewrite_str = str_replace(' ', '-', preg_replace('/[^a-z\d ]/i', '', $name_str));
				$name[$id_lang] = $name_str;
				$description[$id_lang] = $name_str;
				$rewrite[$id_lang] = $rewrite_str;
			}
		}
		$ps_category->name = $name;
		$ps_category->id_walmart = $category->id;
		$ps_category->description = $description;
		$ps_category->link_rewrite = $rewrite;
		$ps_category->id_parent = $parent;
		$ps_category->active = 1;
		$this->current = $category;
		$this->currentIns = $ps_category;
		$ps_category->validateFields();
		$ps_category->save();
		return $ps_category;
	}

	protected function process_categories($categories, $parent = 2) {
		$total = 0;
		foreach($categories as $category) {
			$ps_category = $this->process_category($category, $parent);
			$total += 1;
			if (property_exists($category, 'children')) {
				$total += $this->process_categories($category->children, intval($ps_category->id));
			}
		}
		return $total;
	}

    public function process () {
		$data = $this->get_categories();
		ToolDb::begin();
		try {
			$total = $this->process_categories($data);
			ToolDb::commit();
		} catch(Exception $e) {
			ToolDb::rollback();
		}
    }
}