<?php



class ProcessorProduct{
    protected static $URL_BASE = 'http://api.walmartlabs.com';
    
    protected $current;
    protected $ps_current;
    protected $default_lang;
    protected $processor_categories;
    private $id_processor = [];
    private $api_key;
    private $categories;
    private $configs;
    private $count;
    private $groups = 'full';
    private $page;
    private $supplier;
    private $currentCategories;
    private $treeCategories;
    private $cache_category = [];
    private $prices;
    private $langs;

    public function __construct($api_key, $configs = null) {
        $this->api_key = $api_key;
        $this->configs = $configs ? $configs : [];
        $this->create_meta();
    }

    protected function create_meta() {
        $context = Context::getContext();
        $id = SupplierCore::getIdByName('Walmart');
        if ($id) {
            $this->supplier = new SupplierCore($id);
        } else {
            $this->supplier = new SupplierCore();
            $this->supplier->name = 'Walmart';
            $this->supplier->active = true;
            $this->supplier->save();
        }
        $this->langs = Language::getLanguages(false);
        $this->default_lang = $context->language->id;
        $this->processor_categories = new ProcessorCategory($this->api_key);
        $this->treeCategories = $this->processor_categories->get_categories();
        $this->categories = $this->processor_categories->get_dict();
    }

    protected function create_product($product) {
        $id = ProductWalmart::getByWalmartId($product->itemId);
        $this->currentCategories = [];
        $ps_product = new ProductWalmart($id? $id : null);
        if (property_exists($product, 'brandName')) {
            $ps_product->manufacturer_name = $product->brandName;
            $ps_product->id_manufacturer = (int)$this->process_manufacturer($product->brandName);
        }
        $ps_product->isbn = property_exists($product, 'isbn') ? $product->isbn : null;
        $ps_product->id_supplier = (int)$this->supplier->id;
        $ps_product->supplier_name = $this->supplier->name;
        $ps_product->name = $this->get_name($product, $ps_product);
        $ps_product->id_walmart = $product->itemId;
        $ps_product->description = $this->get_description_long($product, $ps_product);
        $ps_product->description_short = $this->get_description_short($product, $ps_product);
        if (!$ps_product->description_short && $ps_product->description_short = '') {
            $ps_product->description_short = $ps_product->name;
        }
        $this->load_price($ps_product, $product);
        $ps_product->supplier_reference = $product->itemId;
        $ps_product->upc = property_exists($product, 'upc') ? $product->upc : null;
        $ps_product->visibility = 'both';
        $id_category = property_exists($product, 'categoryNode') ? 
            $this->get_category($product->categoryNode) :
            false;
        $ps_product->id_category_default = $id_category ? $id_category : 0;
        $ps_product->deleteCategory(2);
        $ps_product->available_now = 'en stock';
        $ps_product->link_rewrite = $this->get_rewrite($product, $ps_product);
        $ps_product->available_later = 'fuera de stock';
        $ps_product->available_for_order = true;
        $ps_product->is_virtual = 0;
        $ps_product->minimal_quantity = 1;
        $ps_product->out_of_stock = 2;
        $ps_product->active = 1;
        $ps_product->depends_on_stock = 0;
        $this->ps_current = $ps_product;
        $ps_product->save();
        if (count($this->currentCategories)>0) {
            $ps_product->addToCategories($this->currentCategories);
        }
        $ps_product->addSupplierReference($this->supplier->id, 0, $product->itemId, $this->get_cost($product), Currency::getIdByIsoCode('USD'));
        StockAvailableCore::setProductOutOfStock($ps_product->id, 1);
        if ($ps_product->id && $ps_product->id > 0) {
            ProductWalmart::updatePriceShop($ps_product->id, Context::getContext()->shop->id, $this->prices['cost'], $this->prices['price']);
        }
        $this->process_image($ps_product, $product);
        $this->create_product_variant($product, $product->itemId, 1);
        return $ps_product->id;
    }

    protected function create_product_variant($product, $id_parent = null, $default = 0) {
        $id_parent = $id_parent ? $id_parent : $product->parentItemId;
        $id_product = ProductWalmart::getByWalmartId($id_parent);
        if (!$id_product) {
            $data = $this->get_product_walmart($id_parent);
            $id_product = $this->create_product($data);
        }
        $ps_product = new ProductWalmart($id_product);
        $id_variant = Combination::getIdByReference($id_product, $product->itemId);
        $variant = new Combination($id_variant);
        $variant->minimal_quantity = 1;
        $variant->default_on = $default;
        $variant->id_product = $id_product;
        $cost = property_exists($product, 'salePrice') ? round($product->salePrice, 2) : $ps_product->wholesale_price;
        $variant->wholesale_price = $cost;
        $variant->price = $this->calc_sale_price($cost, $ps_product);
        $variant->reference = $product->itemId;
        $variant->supplier_reference = $product->itemId;
        $variant->isbn = property_exists($product, 'isbn') ? $product->isbn : null;
        $variant->upc = property_exists($product, 'upc') ? $product->upc : null;
        $this->ps_current = $variant;
        $variant->save();
        $ps_product->addSupplierReference($this->supplier->id, $variant->id, $product->itemId, $this->get_cost($product), Currency::getIdByIsoCode('USD'));
        $attrs = [];
        $id_attr_color = $this->create_atrribute($product, 'color', 'color');
        $id_attr_size = $this->create_atrribute($product, 'size', 'size');
        if ($id_attr_color) {
            array_push($attrs, $id_attr_color);
        }
        if ($id_attr_size) {
            array_push($attrs, $id_attr_size);
        }
        if (count($attrs) == 0) {
            $variant->delete();
            return;
        }
        $variant->setAttributes($attrs);
        $url = $this->get_image_url($product);
        $images = ToolImage::getImageUrl($ps_product->id, $url);
        if (count($images) > 1) {
            ToolImage::deleteImageByUrl($images, count($images) - 1);
        }
        if (count($images) == 0) {
            $id = $this->process_image($ps_product, $product, false);
        } else {
            $id = $images[0]['id_image'];
        }
        $variant->setImages([$id]);
        
    }

    protected function create_atrribute($product, $field, $group) {
        $name = null;
        $product = get_object_vars($product);
        $ids = [];
        if (isset($product[$field])) {
            $name = substr($product[$field], 0, 128);
        }
        if (!$name) {
            return null;
        }
        $id_group = AttrGroupWalmart::getIdGroupByName($group);
        if (!$id_group) {
            $id_group = AttrGroupWalmart::createGroup($group, $this->langs);
        }
        $id_attribute = AttrWalmart::getAttributeByName($name, $id_group);
        if ($id_attribute) {
            return $id_attribute;
        }
        $name_arr = [];
        foreach($this->langs as $lang) {
            $id_lang = $lang['id_lang'];
            $name_arr[$id_lang] = $name;
        }
        $attr_name = new AttributeCore();
        $attr_name->name = $name_arr;
        $attr_name->id_attribute_group = $id_group;
        $attr_name->save();
        return $attr_name->id;
    }

    protected function get_products() {
        $url_productos = self::$URL_BASE.'/v1/paginated/items?apiKey='.$this->api_key.'&format=json&responseGroup='.$this->groups;
        $data = file_get_contents($url_productos);//llenamos data con el JSON devuelto por la url
        $json = json_decode($data);
        $this->page = $json;
        return $json->items;
    }

    protected function get_products_next() {
        $url_productos = self::$URL_BASE.$this->page->nextPage;
        $data = file_get_contents($url_productos);//llenamos data con el JSON devuelto por la url
        $json = json_decode($data);
        $this->page = $json;
        return $json->items;
    }

    protected function get_product_walmart($id) {
        $url = self::$URL_BASE."/v1/items/$id?format=json&apiKey=$this->api_key";
        $data = file_get_contents($url);
        return json_decode($data);
    }

    protected function process_category($category) {
        $data = $category;
        $parents = explode('_', $data->id);
        $size = count($parents) - 1;
        $last = null;
        $last_id = null;
        for($i = 0; $i < $size; $i += 1) {
            $current = $parents[$i];
            $id = $last_id ? $last_id. '_' : '';
            $id .= $current;
            if (!isset($this->cache_category[$id])) {
                $c = $this->find_by_id($id);
                $last = $this->processor_categories->process_category(
                    $c, $last ? $last->id : 2);
                $this->cache_category[$id] = $last;
            } else {
                $last = $this->cache_category[$id];
            }
            array_push($this->currentCategories, $last->id);
            $last_id = $id;
        }
        if ($last) {
            $id_parent = $last->id;
        }
        $category = $this->processor_categories->process_category($category, $id_parent);
        array_push($this->currentCategories, $category->id);
        return $category->id;
    }

    protected function find_by_id($id, $tree = null, $prefix = null) {
        if (!$tree) {
            $tree = $this->treeCategories;
        }
        $idOriginal = $id;
        $index = strpos($id, '_');
        $idPartial = null;
        if ($index > 0) {
            $idPartial = substr($id, 0, $index);
        } else {
            $idPartial = $id;
        }
        if ($prefix) {
            $idOriginal = $prefix.'_'.$id;
            $idPartial = $prefix.'_'.$idPartial;
        }
        foreach ($tree as $branch) {
            if ($branch->id == $idPartial) {
                if ($idPartial == $idOriginal) {
                    return $branch;
                } else {
                    $res = property_exists($branch, 'children')  ? 
                        $this->find_by_id(substr($id, $index + 1), $branch->children, $idPartial) :
                        $branch;
                    return $res ? $res : $branch;
                }
            } 
        }
        return false;
    }

    protected function get_category($id) {
        $category = $this->find_by_id($id);
        if ($category) {
            return $this->process_category($category);
        }
        return false;
    }
    
    protected function process_manufacturer($manufacturer) {
        $id = ManufacturerCore::getIdByName($manufacturer);
        if (!$id) {
            $man = new ManufacturerCore();
            $man->name = $manufacturer;
            $man->active = true;
            $man->save();
            $id = $man->id;
        } else {
            $man = new ManufacturerCore($id);
            $man->active = true;
            $man->save();
        }
        return $id;
    }

    protected function get_image_url($product) {
        if(property_exists($product, 'largeImage')) {
            return $product->largeImage;
        }
        if(property_exists($product, 'mediumImage')) {
            return $product->mediumImage;
        }
        if(property_exists($product, 'thumbnailImage')) {
            return $product->thumbnailImage;
        }
    }

    protected function save_image($url, $path) {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        if(Tools::copy($url, $tmpfile)) {
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);
                return;
            }
            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            ImageManager::resize(
                $tmpfile,
                $path.'.jpg',
                null,
                null,
                'jpg',
                false,
                $error,
                $tgt_width,
                $tgt_height,
                5,
                $src_width,
                $src_height);
            $types = ImageType::getImagesTypes('products', true);
            foreach($types as $type) {
                ImageManager::resize($tmpfile, $path.'-'.stripslashes($type['name']).'.jpg', $type['width'],
                $type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                $src_width, $src_height);
            }
            @unlink($tmpfile);
        }
    }

    protected function process_image($ps_product, $product, $cover = true) {
        $image = Image::getCover($ps_product->id);
        $url = $this->get_image_url($product);
        if (!$image || !$cover) {
            $image = new Image();
            $image->id_product = $ps_product->id;
            $image->cover = $cover;
            $image->url_external = $url;
            $image->save();
        } else {
            $image = new Image($image['id_image']);
            $image->url_external = $url;
            $image->save();
        }
        $path = $image->getPathForCreation();
        $this->save_image($url, $path);
        return $image->id;
    }

    protected function calc_sale_price($price, $ps_product = null) {
        if ($ps_product) {
            $price = $price - (float)$ps_product->wholesale_price;
        }
        $price = $this->conversion_cost($price);
        $gain = isset($this->configs['gain']) ? $this->configs['gain'] : 0;
        $perc = ($gain / 100) + 1;
        return round($price * $perc, 2);
    }

    private function conversion_cost($cost) {
        if ($this->configs['enableConversion']) {
            $newCost = $cost * $this->configs['rate'];
            $currency = new Currency($this->configs['currency']);
            $cost = $newCost / $currency->conversion_rate;
        }
        return $cost;
    }

    protected function get_cost($product) {
        return property_exists($product, 'salePrice') ? round($product->salePrice, 2) : 0.0;
    }
    
    protected function load_price($ps_product, $product) {
        $cost = $this->get_cost($product);
        $price = 0.0;
        if (property_exists($product, 'salePrice')) {
            $cost = round($product->salePrice, 2);
            $price = round($this->calc_sale_price($cost), 2);
            $this->prices = [
                'price'=>$price,
                'cost'=>$cost
            ];
        }
        $ps_product->base_price = $cost;
        $ps_product->wholesale_price = $cost;
        $ps_product->unit_price = $price;
        $ps_product->price = $price;
        $ps_product->on_sale = 0;
    }

    protected function add_pack($ps_product, $product) {
        $ps_product->advanced_stock_management = 1;
        $id_parent = ProductWalmart::getByParentWalmartId($product->parentItemId);
        if ($id_parent) {
            $parent = new ProductWalmart($id_parent);
            $img = Image::getCover($ps_product->id);
            $parent->updateAttribute(
                $ps_product->id,
                $ps_product->wholesale_price,
                $ps_product->price,
                0,
                0,
                0,
                $img ? $img['id_image'] : null,
                $ps_product->id_walmart,
                "",
                1,
                $ps_product->upc,
                0,
                $ps_product->isbn
            );
        }
        $ps_product->save();
    }

    protected function get_name($product, $ps_product) {
        $name_str = html_entity_decode($product->name);
        if (strpos($name_str, '|') != FALSE) {
            $names = explode('|', $name_str);
            $name_str = $names[0];
        }
        if (strpos($name_str, ':') != FALSE) {
            $names = explode(':', $name_str);
            $name_str = $names[0];
        }
        if (strpos($name_str, '=') != FALSE) {
            $names = explode('=', $name_str);
            $name_str = $names[0];
        }
        if (strlen($name_str) > 128) {
            $name_str = substr($name_str, 0, 128);
        }
        $name = [];
        foreach($this->langs as $lang) {
            $id_lang = $lang['id_lang'];
            if(isset($ps_product->name[$id_lang]) && strlen($ps_product->name[$id_lang])>0) {
                $name[$id_lang] = $ps_product->name[$id_lang];
            } else {
                $name[$id_lang] = $name_str;
            }
        }
        return $name;
    }

    protected function get_rewrite($product, $ps_product) {
        $rewrite = $this->get_name($product, $ps_product);
        $res = [];
        foreach ($rewrite as $key => $value) {
            $res[$key] = Tools::link_rewrite(str_ireplace(' ','_',$value));
        }
        return $res;
    }

    protected function get_description_short($product, $ps_product) {
        if (!property_exists($product, 'shortDescription')) {
            return "";
        }
        $description = [];
        $descr = html_entity_decode(substr($product->shortDescription, 0, 800));
        foreach($this->langs as $lang) {
            $id_lang = $lang['id_lang'];
            if (isset($ps_product->description_short[$id_lang])) {
                $description[$id_lang] = $ps_product->description_short[$id_lang];
            } else {
                $description[$id_lang] = $descr;
            }
        }
        return $description;
    }

    protected function get_description_long($product, $ps_product) {
        $descr = property_exists($product, 'longDescription') ? html_entity_decode($product->longDescription) : '';
        $description = [];
        foreach($this->langs as $lang) {
            $id_lang = $lang['id_lang'];
            if (isset($ps_product->description[$id_lang])) {
                $description[$id_lang] = $ps_product->description[$id_lang];
            } else {
                $description[$id_lang] = $descr;
            }
        }
        return $description;
    }
    
    protected function is_parent($product) {
        return !property_exists($product, 'parentItemId') || $product->parentItemId == $product->itemId;
    }

    protected function processProduct($product) {
        if ($this->is_parent($product)) {
            $this->create_product($product);
        } else {
            $this->create_product_variant($product);
        }
    }

    protected function process_products($products, $max) {
        ToolDb::begin();
        foreach ($products as $product) {
            array_push($this->id_processor, $product->itemId);
            $this->current = $product;
            $this->processProduct($product);
            $this->count += 1;
            if ($this->count >= $max && $max > 0) {
                ToolDb::commit();
                return true;
            }
        }
        ToolDb::commit();
        if ($this->count <= $max && $max > 0 && property_exists($this->page, 'nextPage')) {
           $this->process_products($this->get_products_next(), $max);
        }
    }

    public function start() {
        $this->count = 0;
        $products = $this->get_products(); 
        try {
            $max = array_key_exists('max', $this->configs) ? $this->configs['max'] : -1;
            $this->process_products($products, $max);
            ProductWalmart::activeByListId($this->id_processor);
            ProductWalmart::inactiveByListId($this->id_processor);
            return true;
        } catch (Exception $e) {
            ToolDb::rollback();
            throw $e;
            return false;
        }
        
    }
}