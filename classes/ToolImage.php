<?php

class ToolImage {
    
    public static function getImageUrl($idProduct, $url)
    {
        $db = Db::getInstance();
        $url = $db->escape($url);        
        $sql = new DbQuery();
        $sql->select('`id_image`');
        $sql->from('image');
        $sql->where("id_product = $idProduct and LOWER(`url_external`) = LOWER('$url') and (cover = 0 or cover is null)");
        $sql->orderBy("id_image desc");
        $result =$db->executeS($sql);
        return $result;
    }

    public static function deleteImageByUrl($images, $count)
    {
        $idStr = '';
        for ($i = 0; $i < $count; $i += 1) {
            $idStr .= $i == 0 ? $images[$i]['id_image'] : ','.$images[$i]['id_image'];
        }
        $db = Db::getInstance();
        $table = _DB_PREFIX_.Image::$definition['table'];
        $tableRef = _DB_PREFIX_."product_attribute_image";
        $where = "where id_image in ($idStr)";
        $sql = "delete from $table $where";
        $sqlRef = "delete from $tableRef $where" ;        
        return $db->execute($sqlRef) && $db->execute($sql);
    }
}