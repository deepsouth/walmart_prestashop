<?php

class ToolDb {
    protected static $db;


    protected static function checkDb() {
        if (!self::$db) {
            self::$db = Db::getInstance();
        }
    }

    public static function begin() {
        self::checkDb();
        self::$db->execute('START TRANSACTION');
    }

    public static function commit() {
        self::checkDb();
        self::$db->execute('COMMIT');
    }

    public static function rollback() {
        self::checkDb();
        self::$db->execute('ROLLBACK');
    }

    public static function createField($table, $name, $type = 'varchar(100)', $null = '') {
        self::checkDb();
        $command = "alter table $table add column $name $type $null";
        self::$db->execute($command);
    }

    public static function removeField($table, $name) {
        self::checkDb();
        $command = "alter table $table drop column $name";
        return self::$db->execute($command);
    }

}