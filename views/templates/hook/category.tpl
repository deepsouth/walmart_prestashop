<div class="list-group {if isset($id)}collapse padding-level-down{/if}" {if isset($id)}id="list_{$id}" {/if}>
    {$i=0}
    {foreach $categories as $category}
        {$i=$i+1}
        {$hasChildren=property_exists($category, 'children')}
        <div class="list-group-item" >
            <div {if isset($level)}style="padding-left: {5*$level}%"{/if}>
                <div class="form-inline text-right">
                    <input type="hidden" value="{$category->id}" />
                    <div class="pull-left">
                        <div class="checkbox">
                            <input id="check-{$category->id}" type="checkbox" {if $categoriesSelection[$category->id]['enable']}checked{/if}/>
                        </div>
                        <a {if $hasChildren}role="button" data-toggle="collapse" href="#list_{$category->id}" aria-expanded="false" aria-controls="list_{$category->id}"{/if}>
                            {if isset($nLevel)}{$nLevel}{else}{$nLevel=""}{/if}{$i}. {$category->name}
                        </a>
                        {if $hasChildren}
                            <span class="badge">{count($category->children)}</span>               
                        {/if}
                    </div> 
                    <div class="form-group">
                        <input id="category_gain-{$category->id}" placeholder="% Gain Group" class="form-control input-sm gain-class" value="{$categoriesSelection[$category->id]['gain']}" />
                    </div>
                    <div class="form-group">
                        <input id="category_max-{$category->id}" placeholder="Max Products Group" class="form-control input-sm max-class" value="{$categoriesSelection[$category->id]['max']}" />
                    </div>
                </div>       
            </div>
        </div>
        {if !isset($level)}
            {$level=0}
        {/if}
        {if $hasChildren}
            {include file='./category.tpl' nLevel="$nLevel$i." level=$level+1 id=$category->id categories=$category->children}
        {/if}
    {/foreach}
</div>