<form method="post" class="well" >
    <input type="hidden" name="method" value="config_general" />
    <div class="row">
        <div class="col-md-4 col-sm-12">
            <div class="form-group">
                <label for="key_walmart">{l s="Walmart key" mod="walmartcatalog"}</label>
                <input class="form-control" type="text" id="key_walmart" name="key_walmart" value="{$key}" />
            </div>
            <div class="form-group">
                <label for="gain_perc">{l s="Percent Gain" mod="walmartcatalog"}</label>
                <input class="form-control" type="text" id="gain_perc" name="gain_perc" value="{$gain}" />
            </div>
            <div class="form-group">
                <label>{l s="Max Products Load" mod="walmartcatalog"}</label>
                <input class="form-control" id="max_product" name="max_product" value="{$max}"/>
            </div>
            <div class="form-group">
                <label for="key_walmart">{l s="Rate Tax Default" mod="walmartcatalog"}</label>
                <select id="tax_rate" name="tax_rate">
                    <option value="-1">{l s="Tax not selected" mod="walmartcatalog"}</option>
                    {foreach $taxes as $tax}
                        <option value="{$tax['id_tax']}" {if $taxSelected==$tax['id_tax']}selected{/if}>{$tax['name']}</option>
                    {/foreach}
                </select>
            </div>
             <div class="form-group">
                <label for="filter_load">{l s="Filter Load Product" mod="walmartcatalog"}</label>
                <select id="filter_load" name="filter_load">
                    <option value="0">{l s="All Products" mod="walmartcatalog"}</option>
                    <option value="1">{l s="Only Online" mod="walmartcatalog"}</option>
                    <option value="2">{l s="Only Store" mod="walmartcatalog"}</option>
                </select>
            </div>
            <div class="form-group">
                <input id="enable_conversion" name="enable_conversion" type="checkbox" {if $enableConversion=='on'}checked{/if} />
                <label>{l s="Enable Conversion" mod="walmartcatalog"}</label>
            </div>
            <div {if $enableConversion!='on'}class="collapse"{/if} id="box_conversion">
                <div class="form-group">
                    <label>{l s="Rate Conversion" mod="walmartcatalog"}</label>
                    <input name="rate_conversion" class="form-control" value="{$rate}"/>
                </div>
                <div class="form-group">
                    <label>{l s="Money Conversion" mod="walmartcatalog"}</label>
                    <select name="currency_selected">
                        <option value="-1">{l s="Money not selected" mod="walmartcatalog"}</option>
                        {foreach $currencies as $currency}
                            <option value="{$currency->id}" {if $currencySelected==$currency->id}selected{/if}>{$currency->name}</option>
                        {/foreach}
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-8 col-sm-12">
            {if count($categories)>0}
            <h4 class="list-group-item">
                {l s="Categories" mod="walmartcatalog"}
                <span class="badge">{count($categories)}</span>               
            </h4>
            <input type="hidden" id="category_selected" name="category_selected" value="" />
            <div id="list_category">
                {include file='./category.tpl' categories=$categories}
            </div>
            <script>
                const selection = {json_encode($categoriesSelection)};
                const getId = (id) => {
                    const parts = id.split('-');
                    return parts[parts.length - 1];
                };
                $("#list_category input:checkbox").on('click', (ev) => {
                    let id = getId(ev.target.id);
                    selection[id].enable = ev.target.checked;
                    const checks = $("#list_"+id).find('input:checkbox');
                    const checked = ev.target.checked
                    $(checks).attr('checked', checked);
                    $(checks).each( (index, child) => {
                        let childId = getId(child.id);
                        selection[childId].enable = checked;
                    });
                    $('#category_selected').val(JSON.stringify(selection));
                });
                $("#list_category .gain-class").on('change', (ev) => {
                    let id = ev.target.id;
                    const parts = id.split('-');
                    id = parts[parts.length - 1];
                    selection[id].gain = ev.target.value;
                    $('#category_selected').val(JSON.stringify(selection));
                });
                $("#list_category .max-class").on('change', (ev) => {
                    let id = ev.target.id;
                    const parts = id.split('-');
                    id = parts[parts.length - 1];
                    selection[id].max = ev.target.value;
                    $('#category_selected').val(JSON.stringify(selection));
                });
            </script>
            {else}
                <div class="alert alert-warning">{l s="Error getting categories" mod="walmartcatalog"}</div>
            {/if}
        </div>
    </div>
    <div class="panel-footer">
        <input name="conf_general" type="submit" class="btn btn-default btn-lg" value="{l s="Save" mod="walmartcatalog"}" />
    </div>
    <script>
        $("#enable_conversion").on('change', function(ev) {
            if (ev.target.checked) {
                $("#box_conversion").removeClass('collapse');
            } else {
                $("#box_conversion").addClass('collapse');
            }
        });
    </script>
</form>
