<ul class="nav nav-tabs" role="tablist">
    <li class="nav-item active" role="presentation">
        <a href="#general" aria-controls="general" role="tab" data-toggle="tab">
            {l s="General" mod="walmartcatalog"}
        </a>
    </li>
    <li class="nav-item" role="presentation">
        <a href="#sync" aria-controls="sync" role="tab" data-toggle="tab">
            {l s="Sincronice" mod="walmartcatalog"}
        </a>
    </li>
</ul>