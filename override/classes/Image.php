<?php
ImageCore::$definition['fields']['url_external'] = array('type' => ObjectModel::TYPE_STRING, 'size' => 4096);

class Image extends ImageCore {

    public $url_external;

    public static function inicialiceTable()
    {
        $def = ObjectModel::getDefinition('Image');
        $table = _DB_PREFIX_.Image::$definition['table'];
        return ToolDb::createField($table, 'url_external', 'varchar(4096)');
    }
}