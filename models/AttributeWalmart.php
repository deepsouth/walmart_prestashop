<?php

class AttrWalmart
{
    public static function getAttributeByName($name, $id_group) {
        $db = Db::getInstance();
        $name = $db->escape($name);
        $table_attribute = _DB_PREFIX_."attribute";
        $sql = new DbQuery();
        $sql->select('`id_attribute`');
        $sql->from('attribute_lang');
        
        $sql->where("LOWER(`name`) = LOWER('$name') and `id_attribute` in (select id_attribute from $table_attribute where `id_attribute_group` = $id_group)");
        $result =$db->executeS($sql);
        if ($result) {
            return $result[0]['id_attribute'];
        }
        return null;
    }
}