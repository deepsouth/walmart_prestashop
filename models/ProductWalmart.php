<?php

Product::$definition['fields']['id_walmart'] = array('type' => ObjectModel::TYPE_STRING, 'size' => 30);
Product::$definition['fields']['id_walmart_parent'] = array('type' => ObjectModel::TYPE_STRING, 'size' => 30);

class ProductWalmart extends Product {

    public $id_walmart;
    public $id_walmart_parent;

    public static function getByWalmartId($idWalmart) {
        $sql = new DbQuery();
        $sql->select('`id_product`');
        $sql->from('product');
        $sql->where('`id_walmart` = \''.$idWalmart.'\'');
        $result = Db::getInstance()->executeS($sql);
        if ($result) {
            return $result[0]['id_product'];
        }
        return false;
    }

    public static function getByParentWalmartId($idParentWalmart) {
        $sql = new DbQuery();
        $sql->select('`id_product`');
        $sql->from('product');
        $sql->where('`id_walmart_parent` = \''.$idParentWalmart.'\'');
        $result = Db::getInstance()->executeS($sql);
        if ($result) {
            return $result[0]['id_product'];
        }
        return false;
    }

    public static function inicialiceTable() {
        $def = ObjectModel::getDefinition('Product');
        $table = _DB_PREFIX_.Product::$definition['table'];
        return ToolDb::createField($table, 'id_walmart', 'varchar(30)') &&
            ToolDb::createField($table, 'id_walmart_parent', 'varchar(30)');
    }

    public static function destroyTable() {
        $def = ObjectModel::getDefinition('Product');
        $table = _DB_PREFIX_.Product::$definition['table'];
        return ToolDb::removeField($table, 'id_walmart') &&
            ToolDb::removeField($table, 'id_walmart_parent');
    }
    /**
     * Actualiza el precio de un producto en la tienda
     * @param int $idProduct identificador del producto
     * @param int $idShop identificador de la tienda
     * @param float $cost costo del producto
     * @param float $price precio de venta del producto
     * @return bool retona verdadero si la actualizacion realizada exitosamente
     */
    public static function updatePriceShop($idProduct, $idShop, $cost, $price) {
        $table = "product_shop";
        $where = "id_product = $idProduct and id_shop = $idShop";
        $data = ["price"=>$price, "wholesale_price"=>$cost, "on_sale"=>0];
        return Db::getInstance()->update($table, $data, $where);
    }

    public static function activeByListId($ids) {
        $str_id = '';
        foreach($ids as $id) {
            $str_id .= $str_id == '' ? $id : ", $id";
        }
        $table = "product";
        $tableName = _DB_PREFIX_.$table;
        $where = "id_walmart in ($str_id) and not id_walmart is null";
        $whereShop = "id_product in (select id_product from $tableName where id_walmart in ($str_id) and not id_walmart is null)";
        $data = ["active"=>1];
        return Db::getInstance()->update($table, $data, $where) &&
            Db::getInstance()->update($table."_shop", $data, $whereShop);
    }

    public static function inactiveByListId($ids) {
        $str_id = '';
        foreach($ids as $id) {
            $str_id .= $str_id == '' ? $id : ", $id";
        }
        $table = "product";
        $tableName = _DB_PREFIX_.$table;
        $where = "not id_walmart in ($str_id) and not id_walmart is null";
        $whereShop = "id_product in (select id_product from $tableName where not id_walmart in ($str_id) and not id_walmart is null)";
        $data = ["active"=>0];
        return Db::getInstance()->update($table, $data, $where) &&
            Db::getInstance()->update($table."_shop", $data, $whereShop);
    }
}