<?php

class AttrGroupWalmart
{

    public static function getIdGroupColor() {
        $table_attribute = _DB_PREFIX_."attribute";
        $sql = new DbQuery();
        $sql->select('`id_attribute_group`');
        $sql->from('attribute_group');
        $sql->where("`is_color_group` = 1");
        $result = Db::getInstance()->executeS($sql);
        if ($result) {
            return $result[0]['id_attribute_group'];
        }
        return null;
    }

    public static function getIdGroupByName($name) {
        $table_attribute = _DB_PREFIX_."attribute";
        $db = Db::getInstance();
        $sql = new DbQuery();
        $sql->select('`id_attribute_group`');
        $sql->from('attribute_group_lang');
        $sql->where("LOWER(`name`) = LOWER('".$db->escape($name)."')");
        $result = $db->executeS($sql);
        if ($result) {
            return $result[0]['id_attribute_group'];
        }
        return null;
    }

    public static function createGroup($name, $langs) {
        $name_arr = [];
        foreach ($langs as $lang) {
            $id_lang = $lang['id_lang'];
            $name_arr[$id_lang] = $name;
        }
        $group = new AttributeGroupCore();
        $group->name = $name_arr;
        $group->public_name = $name_arr; 
        $group->is_color_group = 0;
        $group->group_type = 'select';
        $group->save();
        return $group->id;
    }

    public static function createGroupColor() {
        $group = new AttributeGroupCore();
        $group->name = "color";
        $group->public_name = "Color";
        $group->is_color_group = 1;
        $group->save();
        return $group->id;
    }
}