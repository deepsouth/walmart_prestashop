<?php

Category::$definition['fields']['id_walmart'] = array('type' => ObjectModel::TYPE_STRING, 'size' => 30);

class CategoryWalmart extends Category {

    public $id_walmart;

    public static function getByWalmartId($idWalmart) {
        $sql = new DbQuery();
        $sql->select('`id_category`');
        $sql->from('category');
        $sql->where('`id_walmart` = \''.$idWalmart.'\'');
        $result = Db::getInstance()->executeS($sql);
        if ($result) {
            return $result[0]['id_category'];
        }
        return false;
    }

    public static function inicialiceTable() {
        $def = ObjectModel::getDefinition('Category');
        $table = _DB_PREFIX_.Category::$definition['table'];
        return ToolDb::createField($table, 'id_walmart', 'varchar(30)');
    }

    public static function destroyTable() {
        $def = ObjectModel::getDefinition('Category');
        $table = _DB_PREFIX_.Category::$definition['table'];
        return ToolDb::removeField($table, 'id_walmart');
    }
}