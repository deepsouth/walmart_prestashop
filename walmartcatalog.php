<?php

if(!defined('_PS_VERSION_')){
	exit;
}

require_once __DIR__.'/models/CategoryWalmart.php';
require_once __DIR__.'/models/ProductWalmart.php';
require_once __DIR__.'/models/AttributeGroupWalmart.php';
require_once __DIR__.'/models/AttributeWalmart.php';
require_once __DIR__.'/classes/ToolImage.php';
require_once __DIR__.'/classes/Category.php';
require_once __DIR__.'/classes/Product.php';
require_once __DIR__.'/vendor/autoload.php';

use GO\Scheduler;

class Walmartcatalog extends Module{

	private $key;
	private $max;
	private $tax;
	private $groups;
	private $gain;
	private $enableConversion;
	private $rateConversion;
	private $currency;

	public function __construct(){
		$this->name = 'walmartcatalog';
		$this->tab = 'front_office_features';
		$this->version = '1.0.1';
		$this->author = 'Deep South Production & Yan <yan.hernan.551@gmail.com>';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.7.1.0', 'max' => _PS_VERSION_);
		$this->bootstrap = true;
		
		parent::__construct();
		$config = Configuration::getMultiple(
			array(
				'WCURRENCY_SELECTED',
				'WRATE_CONVERSION',
				'WCATALOG_KEY',
				'WENABLE_CONVERSION',
				'WCATALOG_TAX',
				'WCATALOG_CAT_ALLOW',
				'WCATALOG_GAIN',
				'WCATALOG_MAX'));
		$this->key = isset($config['WCATALOG_KEY'])? $config['WCATALOG_KEY'] : '';
		$this->max = isset($config['WCATALOG_MAX'])? $config['WCATALOG_MAX'] : -1;
		$this->tax = isset($config['WCATALOG_TAX'])? $config['WCATALOG_TAX'] : -1;
		$this->gain = isset($config['WCATALOG_GAIN']) ? $config['WCATALOG_GAIN'] : 0;
		$this->enableConversion = isset($config['WENABLE_CONVERSION']) ? $config['WENABLE_CONVERSION'] == 'on': false;
		$this->rateConversion = isset($config['WRATE_CONVERSION']) ? $config['WRATE_CONVERSION']: 0;
		$this->currency = isset($config['WCURRENCY_SELECTED']) ? $config['WCURRENCY_SELECTED']: -1;
		$this->displayName = $this->l('Catalogo de Productos Walmart');
		$this->description = $this->l('Cargar/Actualizar catalogo de productos a traves de las APIS de Walmart.');
	
	}

	public function install(){
		CategoryWalmart::inicialiceTable();
		ProductWalmart::inicialiceTable();
		Image::inicialiceTable();
		if(!parent::install())
			return false;
		Configuration::updateValue('WCATALOG_KEY', '');
		Configuration::updateValue('WCATALOG_TAX', '-1');
		Configuration::updateValue('WCATALOG_CAT_ALLOW', '[]');
		Configuration::updateValue('WCATALOG_HOUR', '0');
		Configuration::updateValue('WCATALOG_GAIN', '0');	
		return true;
	}

	public function uninstall(){
		CategoryWalmart::destroyTable();
		ProductWalmart::destroyTable();
		if(!parent::uninstall())
			return false;
		Configuration::deleteByName('WCATALOG_KEY');
		Configuration::deleteByName('WCATALOG_TAX');
		Configuration::deleteByName('WCATALOG_CAT_ALLOW');
		Configuration::deleteByName('WCATALOG_HOUR');
		Configuration::deleteByName('WCATALOG_GAIN');
		//
		return true;
	}

	public function getFormVariable() {
		$key = Configuration::get('WCATALOG_KEY');
		$gain = Configuration::get('WCATALOG_GAIN');
		$tax = Configuration::get('WCATALOG_TAX');
		$max = Configuration::get('WCATALOG_MAX');
		$rate = Configuration::get('WRATE_CONVERSION');
		$currency_selected = Configuration::get('WCURRENCY_SELECTED');
		$enable = Configuration::get('WENABLE_CONVERSION');
		$selection = Configuration::get('WCATALOG_SELECTION');
		$processor = new ProcessorCategory($key);
		$currencies = Currency::getCurrencies(true);
		$categories = $processor->get_categories();
		if (!$selection) {
			$selection = $processor->inicialice_selection();
		} else {
			$selection = json_decode($selection, true);
		}
		$values = [
			"key" => $key,
			"gain" => $gain,
			"taxSelected" => $tax,
			"max"=>$max,
			"taxes" => Tax::getTaxes(Context::getContext()->language->id),
			"currencies"=> $currencies,
			"rate" => $rate,
			"enableConversion" => $enable,
			"currencySelected" => $currency_selected,
			"categoriesSelection" => $selection,
			"categories" => $categories
		];
		return $values;
	}

	public function getContent(){
		$this->context->controller->addJs($this->_path."views/js/walmartcatalog.js", 'all');
		$this->context->controller->addCSS($this->_path.'views/css/walmartcatalog.css', 'all'); 
		$this->processConfiguration();
		$this->smarty->assign($this->getFormVariable());
		return $this->display(__FILE__,'walmartcatalog.tpl');
	}

	protected function setGeneralData() {
		$key = Tools::getValue('key_walmart');
		$gain = Tools::getValue('gain_perc');
		$tax = Tools::getValue('tax_rate');
		$max = Tools::getValue('max_product');
		$currency = Tools::getValue('currency_selected');
		$rate = Tools::getValue('rate_conversion');
		$enable = Tools::getValue('enable_conversion');
		$cSelection = Tools::getValue('category_selected');
		Configuration::updateValue('WCATALOG_KEY', $key);
		Configuration::updateValue('WCATALOG_GAIN', $gain);
		Configuration::updateValue('WCATALOG_TAX', $tax);
		Configuration::updateValue('WCATALOG_MAX', $max);
		Configuration::updateValue('WCURRENCY_SELECTED', $currency);
		Configuration::updateValue('WRATE_CONVERSION', $rate);
		Configuration::updateValue('WENABLE_CONVERSION', $enable);
		Configuration::updateValue('WCATALOG_SELECTION', $cSelection);
	}

	public function processConfiguration(){
		$method = Tools::getValue('method');
		if($method === 'config_general'){
			$this->setGeneralData();		
			$this->context->smarty->assign('confirmation', 'ok');
		}
		if(Tools::isSubmit('process_product')){
			$products = new ProcessorProduct($this->key, [
				"gain"=>(float)$this->gain,
				"max"=>(int)$this->max,
				"tax"=>(int)$this->tax,
				"enableConversion" => $this->enableConversion,
				"rate"=> (float)$this->rateConversion,
				"currency"=> (int)$this->currency
			]);
			$scheduler = new Scheduler();
			$scheduler->call(
				function ($args) {
					$products = $args['products'];
					$products->start();
				},
				[
					'products' => $products,
				],
				'loadProducts'
			)->output('processing.log');
			$this->context->smarty->assign('confirmation_actual', 'ok');
		}
		if (Tools::isSubmit('process_category')) {
			$pcategory = new ProcessorCategory($this->key);
			$pcategory->process();
		}
	}	
}

