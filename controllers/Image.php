<?php

class ImageController extends AdminImportControllerCore{

    public static function generate_image($entity, $id_entity, $watermark_types, $images_types, $tgt_width, $tgt_height, $path) {
        
        $previous_path = null;
        $path_infos = array();
        $path_infos[] = array($tgt_width, $tgt_height, $path.'.jpg');
        foreach ($images_types as $image_type) {
            $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

            if (ImageManager::resize(
                $tmpfile,
                $path.'-'.stripslashes($image_type['name']).'.jpg',
                $image_type['width'],
                $image_type['height'],
                'jpg',
                false,
                $error,
                $tgt_width,
                $tgt_height,
                5,
                $src_width,
                $src_height
            )) {
                // the last image should not be added in the candidate list if it's bigger than the original image
                if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                    $path_infos[] = array($tgt_width, $tgt_height, $path.'-'.stripslashes($image_type['name']).'.jpg');
                }
                if ($entity == 'products') {
                    if (is_file(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'.jpg')) {
                        unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'.jpg');
                    }
                    if (is_file(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'_'.(int)Context::getContext()->shop->id.'.jpg')) {
                        unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'_'.(int)Context::getContext()->shop->id.'.jpg');
                    }
                }
            }
            if (in_array($image_type['id_image_type'], $watermark_types)) {
                Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
            }
        }
        
    }

}